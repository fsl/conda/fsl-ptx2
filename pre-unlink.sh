if [ -e ${FSLDIR}/share/fsl/sbin/removeFSLWrapper ]; then
  ${FSLDIR}/share/fsl/sbin/removeFSLWrapper \
    probtrackx2 surfmaths surf2surf surf2volume surf_proj \
    label2surf find_the_biggest proj_thresh fdt_matrix_merge \
    probtrackx2_gpu
fi
