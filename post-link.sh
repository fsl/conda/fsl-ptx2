if [ -e ${FSLDIR}/share/fsl/sbin/createFSLWrapper ]; then
  ${FSLDIR}/share/fsl/sbin/createFSLWrapper \
    probtrackx2 surfmaths surf2surf surf2volume surf_proj \
    label2surf find_the_biggest proj_thresh fdt_matrix_merge \
    probtrackx2_gpu
fi
